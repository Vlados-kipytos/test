import logging
import sys
import functools
import warnings
import random


def main():
    # Number 1
    '''
    global year_3
    global month_3
    year_1 = 1997
    month_1 = 7
    year_2 = 2021
    month_2 = 11

    try:
        year_3 = int(input("Введите год: "))
        month_3 = int(input("Введите месяц: "))
        while month_3 < 0 and year_3 < 0 or month_3 > 12:
            year_3 = int(input("Введите год в виде положительного числа: "))
            month_3 = int(input("Введите месяц в виде положительного числа и в диапазоне от (0,12]: "))
    except ValueError:
        logging.error('Введите число')

    if year_1 < year_3 < year_2:
        print('Дата принадлежит диапазону')
        sys.exit()
    elif year_1 == year_3 and month_3 >= month_1:
        print('Дата принадлежит диапазону')
        sys.exit()
    elif year_2 == year_3 and month_3 <= month_2:
        print('Дата принадлежит диапазону')
        sys.exit()
    else:
        print('Дата не принадлежит диапазону')
        sys.exit()
    '''
    # Number 2
    '''
    global N
    comparison_value2 = 0
    try:
        N = int(input("Введите натуральное число:"))
    except ValueError:
        logging.error('Введите натуральное число')

    comparison_value1 = (N * (N + 1)) / 2

    for i in range(N + 1):
        comparison_value2 += i

    if comparison_value1 == comparison_value2:
        print('Теория верна')
    else:
        print('Теория не верна')
    '''
    # Number3
    '''
    def deprecate(msg, klass=PendingDeprecationWarning):
        def decorator(func):
            @functools.wraps(func)
            def wrapper(*args, **kwargs):
                warnings.warn(msg, klass, stacklevel=2)
                return func(*args, **kwargs)

            return wrapper

        return decorator

    class Enum(set):
        def __getattr__(self, value):
            if value in self:
                return value
            raise AttributeError

    class Singleton(object):
        __default = None

        @staticmethod
        def start():
            if Singleton.__default is None:
                Singleton.__default = Singleton()
            return Singleton.__default
    '''
    # Number4
    '''
    M, N = 6, 6

    matrix = [[random.randrange(0, 10) for y in range(M)] for x in range(N)]

    for im in range(N):
        print(matrix[im])

    def Number4(matrix, choise):
        global s
        if choise == 0:
            s = 0
            for i in range(M):
                for j in range(N):
                    if i == j:
                        s += matrix[i][j]
            print('Сумма главной диагонали =', s)
        elif choise == 1:
            s1 = 0
            val = 1
            for im in range(N):
                ls = matrix[im]
                s1 += ls[N - val]
                val += 1
            print('Сумма побочной диагонали =', s1)
        else:
            print('Вы ввели другие значения (не 0 или 1)')

    try:
        global choise
        print('Введите 0 если хотите вычислить сумму главной диагонали, '
              '1 если хотите вычислить сумму побочной диагонали')
        choise = int(input("Введите число: "))
        Number4(matrix, choise)
    except ValueError:
        logging.error('Введите число')
    '''


if __name__ == '__main__':
    main()
